# pytorch-training-monitoring-with-catalyst

This is a code to reproduce the work about deep learning surrogate model training monitoring with ParaView Catalyst, published as:

A. Ribes, F. Mazen, L. Meyer. In Situ Monitoring and Steering Deep Learning Training from Numerical Simulations in ParaView-Catalyst.  Talk at In Situ Infrastructures for Enabling Extreme-Scale Analysis and Visualization (ISAV'22), November 13, 2022, Dallas, TX, USA.

F. Mazen, A. Schieb, A. Ribes, L. Meyer. Visualize, Monitor and Control the Training Process of a Deep Surrogate Model in ParaView. ISC HPC 2022 Project Poster Session. Hamburg, Germany. May-June 2022.

Kitware blog post [here](https://www.kitware.com/deep-learning-surrogate-models-in-paraview-viewing-inference-results-and-monitoring-the-training-process-in-real-time-with-catalyst/)

This repository does not contain the original simulation dataset, which is property of EDF. It has been replaced by a simpler model for this demonstration:
 - 2D regular grid of 10x10 cells (11x11 points)
 - one point field named `f`
 - the field is a 2D gaussian curve parametrized by its center, so 2 floats (X, Y)
 - the simulation dataset is made of fields for X between 0 and 10, and Y between 0 and 10

The pytorch neural network takes the gaussian center as input. It predicts the `f` values at all grid points. The pytorch model is a simple sequence of layers, which works with this simple simulation dataset. You should rework this part to match the problem you want to solve.

## How to use

### Prerequisites

PyTorch is not bundled with official released of ParaView (yet!). You should build ParaView yourself using the python environment from your system in order to use PyTorch from your local installation (`pip` or package manager).

Please follow the instructions in the [Quick guide to using PyTorch in ParaView](https://discourse.paraview.org/t/quick-guide-to-using-pytorch-in-paraview/9037).

You must also deactivate the threading in VTK Python:
 - Set CMake variable `VTK_NO_PYTHON_THREADS` to `OFF`
 - Set CMake variable `VTK_PYTHON_FULL_THREADSAFE` to `ON`

Your ParaView build should also activate Catalyst:
 - build [Catalyst](https://gitlab.kitware.com/paraview/catalyst)
 - activate the CMake variable `PARAVIEW_ENABLE_CATALYST` and set `CATALYST_DIR` to your Catalyst cmake build folder.

### Generate image at each epochs

The first usage is to generate pictures during training. The pytorch training will load the Catalyst-ParaView runtime, and send the grid with prediction to this runtime to generate the pictures.

In a terminal, set the `CATALYST_IMPLEMENTATION_PATHS` environment variable to the folder where the file `libcatalyst-paraview.so` is.
```
export CATALYST_IMPLEMENTATION_PATHS=path/to/paraview/install/lib/catalyst/
```

Then, run the training with a pipeline file that will generate the images, the `pipeline-scripts/pipeline_prediction_objective.py`:
```
python main.py pipeline-scripts/pipeline_prediction_objective.py
```

Once finished, the `datasets` folder should contain one PNG image for each epoch where the top image is the objective and the bottom is the prediction:

![output example](screenshots/RenderView1_000032.png)

### Live Visualization of the training

You can also see the evolution of the training live with ParaView.

 - Launch ParaView
 - In the Catalyst menu, choose Connect... then connect with the default port 22222
 - In the Catalyst menu, choose Pause Simulation
 - In a terminal, set the In a terminal, set the `CATALYST_IMPLEMENTATION_PATHS` environment variable to the folder where the file `libcatalyst-paraview.so` is
 - Then, launch the pytorch training: `python main.py pipeline-scripts/pipeline_prediction_objective.py`
 - In the pipeline browser, the catalyst server should display 3 meshes (objective, prediction and loss), that you can extract, display and explore.
 - To resume the training, go to Catalyst menu and choose Continue

### Parameters Steering

You can steer some training parameters from ParaView GUI. This example shows how to change the learning rate and the prediction location (X/Y) in an interactive way.

 - Launch ParaView
 - In Tools menu, choose Manage Plugins, then load the file `proxies/steering.xml` as a new plugin
 - In the Catalyst menu, choose Connect... then connect with the default port 22222
 - In the Catalyst menu, choose Pause Simulation
 - In a terminal, set the `CATALYST_IMPLEMENTATION_PATHS` environment variable to the folder where the file `libcatalyst-paraview.so` is
 - Then, launch the pytorch training with pipeline script and the xml proxy: `python main.py pipeline-scripts/pipeline_steering.py proxies/steering.xml`
 - In the pipeline browser, the catalyst server should display 3 meshes (objective, prediction and loss).
 - Display the objective and/or the prediction
 - In the `SteeringParameters` filter, change the values of ParameterX and ParameterY, then clic Apply
 
![Steering Parameters](screenshots/SteeringParameters.png)

 - In the Catalyst menu, clic Continue
 - The display of the prediction should have changed according to the new center position
 
![Steering Result](screenshots/SteeringResult.png)

# License

This code is licensed under Apache License, Version 2.0. See NOTICE file and LICENSE file for additional information.

# References

Catalyst 2 documentation: https://catalyst-in-situ.readthedocs.io/en/latest/

Pytorch tutorial: https://pytorch.org/tutorials/beginner/basics/intro.html 

Feel free to reach Kitware at kitware@kitware.com for any question.


