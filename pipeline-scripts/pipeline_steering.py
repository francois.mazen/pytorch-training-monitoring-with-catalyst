from paraview.simple import *
from paraview import catalyst

print("executing catalyst_pipeline")

producer_pred = TrivialProducer(registrationName="prediction")
producer_obj = TrivialProducer(registrationName="objective")
producer_loss = TrivialProducer(registrationName="loss")

# Steerable parameters
steerable_parameters = CreateSteerableParameters("SteerableParameters")

options = catalyst.Options()
options.EnableCatalystLive = 1

def catalyst_execute(info):
    # Force pipeline update because we have no extractors.
    producer_pred.UpdatePipeline()
    producer_obj.UpdatePipeline()
    producer_loss.UpdatePipeline()
