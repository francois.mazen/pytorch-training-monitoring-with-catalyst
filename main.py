import torch
import sys
import os

from SimulationDataset import SimulationDataset
from SurrogateModel import SurrogateModelNeuralNetwork
from SurrogateModelTrainer import SurrogateModelTrainer
from CatalystAdaptor import CatalystAdaptor

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

simulation_dataset = SimulationDataset(device)
print(f"feature count: {simulation_dataset.input_size()}")
print(f"output size (grid): {simulation_dataset.output_size()}")
print(f"simulation samples: {len(simulation_dataset)}")

model = SurrogateModelNeuralNetwork(simulation_dataset.input_size(), simulation_dataset.output_size()).to(device)
print(model)

validation_ratio = 0.2
learning_rate = 1e-4
prediction_x = 5
prediction_y = 5
batch_size = 64
epochs = 200
trainer = SurrogateModelTrainer(simulation_dataset, validation_ratio, learning_rate, batch_size, model, device)

adaptor = CatalystAdaptor()

catalyst_pipeline_scripts = []
catalyst_steering_proxies = []
uses_steering = False
for filepath in sys.argv[1:]:
    assert os.path.isfile(filepath)
    if filepath.split('.')[1] == 'xml':
        catalyst_steering_proxies.append(filepath)
        uses_steering = True
    else:
        catalyst_pipeline_scripts.append(filepath)
adaptor.initialize(catalyst_pipeline_scripts, catalyst_steering_proxies)

for t in range(epochs):
    print(f"Epoch {t+1}\n-------------------------------")
    trainer.train_loop()
    trainer.test_loop()
    prediction = trainer.get_prediction(prediction_x, prediction_y)
    objective = simulation_dataset.compute_values(prediction_x, prediction_y)
    loss_history = trainer.loss_history
    adaptor.execute(prediction, objective, loss_history, t, learning_rate, prediction_x, prediction_y)
    if uses_steering:
        previous_learning_rate = learning_rate
        learning_rate, prediction_x, prediction_y = adaptor.results()
        if learning_rate != previous_learning_rate:
            trainer.optimizer.param_groups[0]['lr'] = learning_rate

adaptor.finalize()
print("Done!")

