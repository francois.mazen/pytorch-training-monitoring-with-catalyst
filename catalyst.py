##############
# This is an experimental python wrapper of Catalyst2 C API
#
# DO NOT USE IN PRODUCTION
#
##############

import ctypes
import os
import numpy


# -----------------------
class node(dict):
    def __missing__(self, key):
        value = self[key] = type(self)()
        return value

    def asDict(self):
        for k, v in self.items():
            if type(v) is type(self):
                self[k] = v.asDict()
        return dict(self)


# -----------------------
# Import the catalyst library.
catalyst_path = os.getenv('CATALYST_IMPLEMENTATION_PATHS')
if not catalyst_path:
    raise Exception('CATALYST_IMPLEMENTATION_PATHS environment variable not set')

cl = ctypes.CDLL(catalyst_path+"/libcatalyst-paraview.so")

# -----------------------
# conduit node API
# # create node
cl.catalyst_conduit_node_create.restype = ctypes.c_void_p

# # set double node
cl.catalyst_conduit_node_set_double.argtypes = [
    ctypes.c_void_p, ctypes.c_double]

# # get double node
cl.catalyst_conduit_node_as_double.argtypes = [ctypes.c_void_p]
cl.catalyst_conduit_node_as_double.restype = ctypes.c_double

cl.catalyst_conduit_node_fetch_path_as_double.argtypes = [
    ctypes.c_void_p, ctypes.c_char_p]
cl.catalyst_conduit_node_fetch_path_as_double.restype = ctypes.c_double


# # node printers
cl.catalyst_conduit_node_print_detailed.argtypes = [
    ctypes.c_void_p]
cl.catalyst_conduit_node_print.argtypes = [ctypes.c_void_p]

# # setters to specific path in node
cl.catalyst_conduit_node_set_path_double.argtypes = [
    ctypes.c_void_p, ctypes.c_char_p, ctypes.c_double]
cl.catalyst_conduit_node_set_path_char8_str.argtypes = [
    ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
cl.catalyst_conduit_node_set_path_int32.argtypes = [
    ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int32]


# # array setters
# # # We need one more level of abstraction for array setters to handle variable input_list sizes.
def setDoubleArray(input_node, path, input_list):
    # define (or overwrite) argtypes
    cl.catalyst_conduit_node_set_path_double_ptr.argtypes = [
        ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_double), ctypes.c_int]
    # call the function
    cl.catalyst_conduit_node_set_path_double_ptr(
        input_node, path.encode(), (ctypes.c_double*len(input_list))(*input_list), len(input_list))
    return input_node


def setFloatArray(input_node, path, input_list):
    # define (or overwrite) argtypes
    cl.catalyst_conduit_node_set_path_float_ptr.argtypes = [
        ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_float), ctypes.c_int]
    # call the function
    cl.catalyst_conduit_node_set_path_float_ptr(
        input_node, path.encode(), (ctypes.c_float*len(input_list))(*input_list), len(input_list))
    return input_node


def setIntArray(input_node, path, input_list):
    # define (or overwrite) argtypes
    cl.catalyst_conduit_node_set_path_int_ptr.argtypes = [
        ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_int), ctypes.c_int]
    # call the function
    cl.catalyst_conduit_node_set_path_int_ptr(
        input_node, path.encode(), (ctypes.c_int*len(input_list))(*input_list), len(input_list))
    return input_node


# # node meta information
cl.catalyst_conduit_node_number_of_children.argtypes = [ctypes.c_void_p]
cl.catalyst_conduit_node_number_of_children.restype = ctypes.c_int
cl.catalyst_conduit_node_number_of_elements.argtypes = [ctypes.c_void_p]
cl.catalyst_conduit_node_number_of_elements.restype = ctypes.c_int

# # get child node by index
cl.catalyst_conduit_node_child.argtypes = [ctypes.c_void_p, ctypes.c_int]
cl.catalyst_conduit_node_child.restype = ctypes.c_void_p

# -----------------------
# catalyst API
cl.catalyst_initialize.argtypes = [ctypes.c_void_p]
cl.catalyst_execute.argtypes = [ctypes.c_void_p]
cl.catalyst_finalize.argtypes = [ctypes.c_void_p]
cl.catalyst_results.argtypes = [ctypes.c_void_p]
cl.catalyst_about.argtypes = [ctypes.c_void_p]


# -----------------------
def FillNodeFromDict(node, input_dict, cumul_path=""):
    """Recursively fills node values from a python dictionnary"""
    for key, value in input_dict.items():
        if (type(value) is dict or type(value) is node):
            FillNodeFromDict(node, value, cumul_path=cumul_path+key+'/')
        else:
            if type(value) is int:
                r = cl.catalyst_conduit_node_set_path_int32(
                    node,  str(
                        cumul_path + key).encode(), value)
            elif type(value) is float:
                # Python floats are C doubles.
                r = cl.catalyst_conduit_node_set_path_double(
                    node,  str(
                        cumul_path + key).encode(), value)
            elif type(value) is str:
                r = cl.catalyst_conduit_node_set_path_char8_str(
                    node, str(
                        cumul_path + key).encode(), value.encode())
            elif type(value) is list:
                if type(value[0]) is float:
                    node = setDoubleArray(node, str(cumul_path + key), value)
                elif type(value[0]) is int:
                    node = setIntArray(node, str(cumul_path + key), value)
                elif type(value[0]) is numpy.float32:
                    node = setFloatArray(node, str(cumul_path + key), value)
                else:
                    raise TypeError(
                        f"Unknown list type {type(value[0])} for value {value[0]} at path {cumul_path+key}")
            else:
                raise TypeError(
                    f"Unknown data type {type(value)} for value {value} at path {cumul_path+key}")
    return


# High-level Catalyst2.0 functions wrapping
# -----------------------
def initialize(input_dict, verbose=False):
    """Loads the paraview implementation"""
    initialize_node = cl.catalyst_conduit_node_create()
    FillNodeFromDict(initialize_node, input_dict)
    if verbose:
        cl.catalyst_conduit_node_print(initialize_node)
    return cl.catalyst_initialize(initialize_node)


# -----------------------
def execute(input_dict, verbose=False):
    n = cl.catalyst_conduit_node_create()
    FillNodeFromDict(n, input_dict)
    if verbose:
        cl.catalyst_conduit_node_print(n)
    return cl.catalyst_execute(n)


# -----------------------
def about():
    about_node = cl.catalyst_conduit_node_create()
    r = cl.catalyst_about(about_node)
    cl.catalyst_conduit_node_print(about_node)
    return r


# -----------------------
def results():
    node = cl.catalyst_conduit_node_create()
    cl.catalyst_results(node)
    # cl.catalyst_conduit_node_print(node)
    lr = cl.catalyst_conduit_node_fetch_path_as_double(
        node, "catalyst/steerable/fields/learningrate/values".encode())
    parameter_x = cl.catalyst_conduit_node_fetch_path_as_double(
        node, "catalyst/steerable/fields/parameter_x/values".encode())
    parameter_y = cl.catalyst_conduit_node_fetch_path_as_double(
        node, "catalyst/steerable/fields/parameter_y/values".encode())
    # TODO: Steering: return node content properly parsed.
    return [lr, parameter_x, parameter_y]


# -----------------------
def finalize():
    finalize_node = cl.catalyst_conduit_node_create()
    return cl.catalyst_finalize(finalize_node)
