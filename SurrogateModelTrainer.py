import torch
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import numpy as np


class SurrogateModelTrainer():

    def __init__(self, simulation_dataset, validation_ratio, learning_rate, batch_size, model, device):
        validation_size = int(len(simulation_dataset) * validation_ratio)
        train_size = len(simulation_dataset) - validation_size
        print(f"train size: {train_size}")
        print(f"test size: {validation_size}")
        train_dataset, test_dataset = random_split(simulation_dataset, [train_size, validation_size], generator=torch.Generator().manual_seed(42))
        
        self.train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        self.test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)
        self.loss_function = nn.MSELoss()
        self.optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
        self.model = model
        self.loss_history = []
        self.device = device


    def train_loop(self):
        size = len(self.train_dataloader.dataset)
        for batch, (X, y) in enumerate(self.train_dataloader):
            # Compute prediction and loss
            pred = self.model(X)
            loss = self.loss_function(pred, y)

            # Backpropagation
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            if batch % 100 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")


    def test_loop(self):
        size = len(self.test_dataloader.dataset)
        num_batches = len(self.test_dataloader)
        test_loss, correct = 0, 0

        with torch.no_grad():
            for X, y in self.test_dataloader:
                pred = self.model(X)
                test_loss += self.loss_function(pred, y).item()
                # Not good for our model, should be changed:
                correct += (pred.argmax(1) == y.argmax(1)).type(torch.float).sum().item()

        test_loss /= num_batches
        correct /= size
        print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
        self.loss_history.append(test_loss)
    
    def get_prediction(self, x, y):
        with torch.no_grad():
            return self.model(torch.tensor([np.float32(x),np.float32(y)]).to(self.device))

