from torch import nn

class SurrogateModelNeuralNetwork(nn.Module):
    '''Very simple neural network for surrogate model.'''

    def __init__(self, input_size, output_size):
        super(SurrogateModelNeuralNetwork, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(input_size, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, output_size),
        )

    def forward(self, x):
        output = self.linear_relu_stack(x)
        return output
