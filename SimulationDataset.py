import torch
from torch.utils.data import Dataset
import numpy as np
import math

class SimulationDataset(Dataset):
    '''Toy simulation dataset that compute a function over a set of spatial parameters.'''

    def __init__(self, device):
        self.x_min = 0
        self.x_max = 10
        self.x_samples = 15
        self.y_min = 0
        self.y_max = 10
        self.y_samples = 15
        self.features = [torch.tensor([np.float32(x),np.float32(y)]).to(device) \
                         for x in np.linspace(self.x_min, self.x_max, self.x_samples) \
                         for y in np.linspace(self.y_min, self.y_max, self.y_samples)]
        self.target_grid_x_dim = 10
        self.target_grid_y_dim = 10
        self.targets = []
        for feature in self.features:
            grid_values = self.compute_values(feature[0], feature[1])
            self.targets.append(torch.tensor(grid_values).to(device))

    def __len__(self):
        return len(self.features)

    def __getitem__(self, idx):
        return self.features[idx], self.targets[idx]
    
    def compute(self, x_feature, y_feature, x, y):
        # 2D Gaussian centered at x_feature, y_feature
        return np.float32(math.exp(-(((x - x_feature)**2 + (y - y_feature)**2)/30)))
        
    def compute_values(self, x_feature, y_feature):
        return [self.compute(x_feature, y_feature, x, y) \
                for x in range(self.target_grid_x_dim + 1) \
                for y in range(self.target_grid_y_dim + 1)]

    def input_size(self):
        return len(self.features[0])

    def output_size(self):
        return len(self.targets[0])
