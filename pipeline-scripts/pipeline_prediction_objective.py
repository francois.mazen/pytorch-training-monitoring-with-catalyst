# script-version: 2.0
# Catalyst state generated using paraview version 5.11.0-RC1-144-g016020f90f

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1043, 893]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [5.0, 10.5, 0.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [5.0, 10.5, 44.93372326988676]
renderView1.CameraFocalPoint = [5.0, 10.5, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 11.629703349613008
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1043, 893)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'PVTrivialProducer'
extractobjective = PVTrivialProducer(registrationName='objective')

# create a new 'PVTrivialProducer'
extractprediction = PVTrivialProducer(registrationName='prediction')

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=extractprediction)
contour1.ContourBy = ['POINTS', 'f']
contour1.Isosurfaces = [0.0, 0.1111111111111111, 0.2222222222222222, 0.3333333333333333, 0.4444444444444444, 0.5555555555555556, 0.6666666666666666, 0.7777777777777777, 0.8888888888888888, 1.0]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Contour'
contour2 = Contour(registrationName='Contour2', Input=extractobjective)
contour2.ContourBy = ['POINTS', 'f']
contour2.Isosurfaces = [0.0, 0.1111111111111111, 0.2222222222222222, 0.3333333333333333, 0.4444444444444444, 0.5555555555555556, 0.6666666666666666, 0.7777777777777777, 0.8888888888888888, 1.0]
contour2.PointMergeMethod = 'Uniform Binning'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from extractprediction
extractpredictionDisplay = Show(extractprediction, renderView1, 'UniformGridRepresentation')

# get 2D transfer function for 'f'
fTF2D = GetTransferFunction2D('f')
fTF2D.ScalarRangeInitialized = 1
fTF2D.Range = [0.18887560069561005, 1.0, 0.0, 1.0]

# get color transfer function/color map for 'f'
fLUT = GetColorTransferFunction('f')
fLUT.TransferFunction2D = fTF2D
fLUT.RGBPoints = [-0.00021846871823072433, 0.0, 0.0, 0.34902, 0.031038358429213986, 0.039216, 0.062745, 0.380392, 0.062295185576658696, 0.062745, 0.117647, 0.411765, 0.0935520127241034, 0.090196, 0.184314, 0.45098, 0.12480883987154812, 0.12549, 0.262745, 0.501961, 0.15606566701899283, 0.160784, 0.337255, 0.541176, 0.18732249416643754, 0.2, 0.396078, 0.568627, 0.21857932131388225, 0.239216, 0.454902, 0.6, 0.24983614846132696, 0.286275, 0.521569, 0.65098, 0.28109297560877167, 0.337255, 0.592157, 0.701961, 0.3123498027562164, 0.388235, 0.654902, 0.74902, 0.3436066299036611, 0.466667, 0.737255, 0.819608, 0.3748634570511058, 0.572549, 0.819608, 0.878431, 0.4061202841985505, 0.654902, 0.866667, 0.909804, 0.4373771113459952, 0.752941, 0.917647, 0.941176, 0.4686339384934399, 0.823529, 0.956863, 0.968627, 0.49989076564088464, 0.941176, 0.984314, 0.988235, 0.49989076564088464, 0.988235, 0.960784, 0.901961, 0.5198951350152493, 0.988235, 0.945098, 0.85098, 0.5398995043896139, 0.980392, 0.898039, 0.784314, 0.5624044199357741, 0.968627, 0.835294, 0.698039, 0.5936612470832188, 0.94902, 0.733333, 0.588235, 0.6249180742306635, 0.929412, 0.65098, 0.509804, 0.6561749013781082, 0.909804, 0.564706, 0.435294, 0.6874317285255529, 0.878431, 0.458824, 0.352941, 0.7186885556729976, 0.839216, 0.388235, 0.286275, 0.7499453828204423, 0.760784, 0.294118, 0.211765, 0.781202209967887, 0.701961, 0.211765, 0.168627, 0.8124590371153317, 0.65098, 0.156863, 0.129412, 0.8437158642627764, 0.6, 0.094118, 0.094118, 0.8749726914102212, 0.54902, 0.066667, 0.098039, 0.9062295185576659, 0.501961, 0.05098, 0.12549, 0.9374863457051106, 0.45098, 0.054902, 0.172549, 0.9687431728525553, 0.4, 0.054902, 0.192157, 1.0, 0.34902, 0.070588, 0.211765]
fLUT.ColorSpace = 'Lab'
fLUT.NanColor = [0.25, 0.0, 0.0]
fLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'f'
fPWF = GetOpacityTransferFunction('f')
fPWF.Points = [-0.00021846871823072433, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
fPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
extractpredictionDisplay.Representation = 'Surface'
extractpredictionDisplay.ColorArrayName = ['POINTS', 'f']
extractpredictionDisplay.LookupTable = fLUT
extractpredictionDisplay.SelectTCoordArray = 'None'
extractpredictionDisplay.SelectNormalArray = 'None'
extractpredictionDisplay.SelectTangentArray = 'None'
extractpredictionDisplay.OSPRayScaleArray = 'f'
extractpredictionDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
extractpredictionDisplay.SelectOrientationVectors = 'None'
extractpredictionDisplay.SelectScaleArray = 'None'
extractpredictionDisplay.GlyphType = 'Arrow'
extractpredictionDisplay.GlyphTableIndexArray = 'None'
extractpredictionDisplay.GaussianRadius = 0.05
extractpredictionDisplay.SetScaleArray = ['POINTS', 'f']
extractpredictionDisplay.ScaleTransferFunction = 'PiecewiseFunction'
extractpredictionDisplay.OpacityArray = ['POINTS', 'f']
extractpredictionDisplay.OpacityTransferFunction = 'PiecewiseFunction'
extractpredictionDisplay.DataAxesGrid = 'GridAxesRepresentation'
extractpredictionDisplay.PolarAxes = 'PolarAxesRepresentation'
extractpredictionDisplay.ScalarOpacityUnitDistance = 3.0468307578901657
extractpredictionDisplay.ScalarOpacityFunction = fPWF
extractpredictionDisplay.TransferFunction2D = fTF2D
extractpredictionDisplay.OpacityArrayName = ['POINTS', 'f']
extractpredictionDisplay.ColorArray2Name = ['POINTS', 'f']
extractpredictionDisplay.SliceFunction = 'Plane'
extractpredictionDisplay.SelectInputVectors = [None, '']
extractpredictionDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractpredictionDisplay.ScaleTransferFunction.Points = [-0.00021846871823072433, 0.0, 0.5, 0.0, 0.8329452872276306, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractpredictionDisplay.OpacityTransferFunction.Points = [-0.00021846871823072433, 0.0, 0.5, 0.0, 0.8329452872276306, 1.0, 0.5, 0.0]

# init the 'Plane' selected for 'SliceFunction'
extractpredictionDisplay.SliceFunction.Origin = [5.0, 5.0, 0.0]

# show data from extractobjective
extractobjectiveDisplay = Show(extractobjective, renderView1, 'UniformGridRepresentation')

# trace defaults for the display properties.
extractobjectiveDisplay.Representation = 'Surface'
extractobjectiveDisplay.ColorArrayName = ['POINTS', 'f']
extractobjectiveDisplay.LookupTable = fLUT
extractobjectiveDisplay.SelectTCoordArray = 'None'
extractobjectiveDisplay.SelectNormalArray = 'None'
extractobjectiveDisplay.SelectTangentArray = 'None'
extractobjectiveDisplay.Position = [0.0, 11.0, 0.0]
extractobjectiveDisplay.OSPRayScaleArray = 'f'
extractobjectiveDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
extractobjectiveDisplay.SelectOrientationVectors = 'None'
extractobjectiveDisplay.SelectScaleArray = 'None'
extractobjectiveDisplay.GlyphType = 'Arrow'
extractobjectiveDisplay.GlyphTableIndexArray = 'None'
extractobjectiveDisplay.GaussianRadius = 0.05
extractobjectiveDisplay.SetScaleArray = ['POINTS', 'f']
extractobjectiveDisplay.ScaleTransferFunction = 'PiecewiseFunction'
extractobjectiveDisplay.OpacityArray = ['POINTS', 'f']
extractobjectiveDisplay.OpacityTransferFunction = 'PiecewiseFunction'
extractobjectiveDisplay.DataAxesGrid = 'GridAxesRepresentation'
extractobjectiveDisplay.PolarAxes = 'PolarAxesRepresentation'
extractobjectiveDisplay.ScalarOpacityUnitDistance = 3.0468307578901657
extractobjectiveDisplay.ScalarOpacityFunction = fPWF
extractobjectiveDisplay.TransferFunction2D = fTF2D
extractobjectiveDisplay.OpacityArrayName = ['POINTS', 'f']
extractobjectiveDisplay.ColorArray2Name = ['POINTS', 'f']
extractobjectiveDisplay.SliceFunction = 'Plane'
extractobjectiveDisplay.SelectInputVectors = [None, '']
extractobjectiveDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractobjectiveDisplay.ScaleTransferFunction.Points = [0.18887560069561005, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractobjectiveDisplay.OpacityTransferFunction.Points = [0.18887560069561005, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
extractobjectiveDisplay.PolarAxes.Translation = [0.0, 11.0, 0.0]

# init the 'Plane' selected for 'SliceFunction'
extractobjectiveDisplay.SliceFunction.Origin = [5.0, 5.0, 0.0]

# show data from contour1
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.ColorArrayName = ['POINTS', '']
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'None'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'f'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'None'
contour1Display.SelectScaleArray = 'f'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'f'
contour1Display.GaussianRadius = 0.05
contour1Display.SetScaleArray = ['POINTS', 'f']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'f']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'
contour1Display.SelectInputVectors = [None, '']
contour1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [0.2222222222222222, 0.0, 0.5, 0.0, 0.8888888888888888, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [0.2222222222222222, 0.0, 0.5, 0.0, 0.8888888888888888, 1.0, 0.5, 0.0]

# show data from contour2
contour2Display = Show(contour2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour2Display.Representation = 'Surface'
contour2Display.ColorArrayName = ['POINTS', '']
contour2Display.SelectTCoordArray = 'None'
contour2Display.SelectNormalArray = 'None'
contour2Display.SelectTangentArray = 'None'
contour2Display.Position = [0.0, 11.0, 0.0]
contour2Display.OSPRayScaleArray = 'f'
contour2Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour2Display.SelectOrientationVectors = 'None'
contour2Display.SelectScaleArray = 'f'
contour2Display.GlyphType = 'Arrow'
contour2Display.GlyphTableIndexArray = 'f'
contour2Display.GaussianRadius = 0.05
contour2Display.SetScaleArray = ['POINTS', 'f']
contour2Display.ScaleTransferFunction = 'PiecewiseFunction'
contour2Display.OpacityArray = ['POINTS', 'f']
contour2Display.OpacityTransferFunction = 'PiecewiseFunction'
contour2Display.DataAxesGrid = 'GridAxesRepresentation'
contour2Display.PolarAxes = 'PolarAxesRepresentation'
contour2Display.SelectInputVectors = [None, '']
contour2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour2Display.ScaleTransferFunction.Points = [0.2222222238779068, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour2Display.OpacityTransferFunction.Points = [0.2222222238779068, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
contour2Display.PolarAxes.Translation = [0.0, 11.0, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for fLUT in view renderView1
fLUTColorBar = GetScalarBar(fLUT, renderView1)
fLUTColorBar.Title = 'f'
fLUTColorBar.ComponentTitle = ''

# set color bar visibility
fLUTColorBar.Visibility = 1

# show color legend
extractpredictionDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
extractobjectiveDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup extractors
# ----------------------------------------------------------------

# create extractor
pNG1 = CreateExtractor('PNG', renderView1, registrationName='PNG1')
# trace defaults for the extractor.
pNG1.Trigger = 'TimeStep'

# init the 'PNG' selected for 'Writer'
pNG1.Writer.FileName = 'RenderView1_{timestep:06d}{camera}.png'
pNG1.Writer.ImageResolution = [1043, 893]
pNG1.Writer.Format = 'PNG'

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pNG1)
# ----------------------------------------------------------------

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.EnableCatalystLive = 1
options.CatalystLiveTrigger = 'TimeStep'

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    from paraview.simple import SaveExtractsUsingCatalystOptions
    # Code for non in-situ environments; if executing in post-processing
    # i.e. non-Catalyst mode, let's generate extracts using Catalyst options
    SaveExtractsUsingCatalystOptions(options)
