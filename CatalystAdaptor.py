import catalyst
from catalyst import node
import os


class CatalystAdaptor():
    def __init__(self):
        self.catalyst_path = os.getenv('CATALYST_IMPLEMENTATION_PATHS')
        if not self.catalyst_path:
            raise Exception('CATALYST_IMPLEMENTATION_PATHS environment variable not set')

    def initialize(self, pipeline_script_filepaths, xml_proxy_filepaths, verbose=False):
        '''Initialize the catalyst engine.
           Expected arguments are pipeline scripts (python), and proxy scripts (xml) for steering.'''
        n = node()

        # Add scripts and proxies
        count = 0
        for pipeline in pipeline_script_filepaths:
            assert os.path.isfile(pipeline)
            n["catalyst"]["scripts"]["script"+str(count)] = pipeline
            count += 1
        for proxy in xml_proxy_filepaths:
            assert os.path.isfile(proxy)
            n["catalyst"]["proxies"]["proxy"+str(count)] = proxy
            count += 1

        # ParaView implementation
        n["catalyst_load"]["implementation"] = "paraview"
        n["catalyst_load"]["search_paths"]["paraview"] = self.catalyst_path
        return catalyst.initialize(n.asDict(), verbose=verbose)


    def finalize(self):
        '''Finalize the catalyst instance, which should not be called before the next call to initialize.'''
        return catalyst.finalize()


    def about(self):
        '''Returns information about the current catalyst implementation.'''
        return catalyst.about()


    def results(self):
        '''Returns the steering parameters with values from the ParaView GUI.'''
        [learning_rate, parameter_x, parameter_y] = catalyst.results()
        print(f"learning rate: {learning_rate}")
        print(f"parameter X: {parameter_x}")
        print(f"parameter Y: {parameter_y}")
        return [learning_rate, parameter_x, parameter_y]


    def execute(self, prediction_values, objective_values, loss_history, epoch, learning_rate, parameter_x, parameter_y):

        # https://llnl-conduit.readthedocs.io/en/latest/blueprint_mesh.html#uniform

        mesh_prediction = node()
        mesh_prediction["coordsets"]["my_coords"]["type"] = "uniform"
        mesh_prediction["coordsets"]["my_coords"]["dims"]["i"] = 11
        mesh_prediction["coordsets"]["my_coords"]["dims"]["j"] = 11
        mesh_prediction["coordsets"]["my_coords"]["origins"]["x"] = 0
        mesh_prediction["coordsets"]["my_coords"]["origins"]["y"] = 0
        mesh_prediction["coordsets"]["my_coords"]["spacing"]["dx"] = 1
        mesh_prediction["coordsets"]["my_coords"]["spacing"]["dy"] = 1
        

        mesh_prediction["topologies"]["my_topo"]["type"] = "uniform"
        mesh_prediction["topologies"]["my_topo"]["coordset"] = "my_coords"


        mesh_prediction["fields"]["f"]["association"] = "vertex"
        mesh_prediction["fields"]["f"]["topology"] = "my_topo"
        mesh_prediction["fields"]["f"]["volume_dependent"] = "false"
        mesh_prediction["fields"]["f"]["values"] = prediction_values.tolist()

# ========================================================================

        mesh_objective = node()
        mesh_objective["coordsets"]["my_coords"]["type"] = "uniform"
        mesh_objective["coordsets"]["my_coords"]["dims"]["i"] = 11
        mesh_objective["coordsets"]["my_coords"]["dims"]["j"] = 11
        mesh_objective["coordsets"]["my_coords"]["origins"]["x"] = 0
        mesh_objective["coordsets"]["my_coords"]["origins"]["y"] = 0
        mesh_objective["coordsets"]["my_coords"]["spacing"]["dx"] = 1
        mesh_objective["coordsets"]["my_coords"]["spacing"]["dy"] = 1
        

        mesh_objective["topologies"]["my_topo"]["type"] = "uniform"
        mesh_objective["topologies"]["my_topo"]["coordset"] = "my_coords"


        mesh_objective["fields"]["f"]["association"] = "vertex"
        mesh_objective["fields"]["f"]["topology"] = "my_topo"
        mesh_objective["fields"]["f"]["volume_dependent"] = "false"
        mesh_objective["fields"]["f"]["values"] = objective_values

# ========================================================================

        mesh_loss = node()
        mesh_loss["coordsets"]["coords"]["type"] = "uniform"
        mesh_loss["coordsets"]["coords"]["dims"]["i"] = 2
        mesh_loss["coordsets"]["coords"]["dims"]["j"] = len(loss_history)+1
        mesh_loss["coordsets"]["coords"]["origin"]["x"] = -2
        mesh_loss["coordsets"]["coords"]["origin"]["y"] = -2
        mesh_loss["coordsets"]["coords"]["spacing"]["dx"] = 1
        mesh_loss["coordsets"]["coords"]["spacing"]["dy"] = 1
    
        mesh_loss["topologies"]["mesh"]["type"] = "uniform"
        mesh_loss["topologies"]["mesh"]["coordset"] = "coords"
    
        mesh_loss["fields"]["loss"]["association"] = "element"
        mesh_loss["fields"]["loss"]["topology"] = "mesh"
        mesh_loss["fields"]["loss"]["volume_dependent"] = "false"
        mesh_loss["fields"]["loss"]["values"] = loss_history
    
# ========================================================================

        mesh_steerable = node()
        mesh_steerable["coordsets"]["coords"]["type"] = "explicit"
        mesh_steerable["coordsets"]["coords"]["values"]["x"] = [1.0]
        mesh_steerable["coordsets"]["coords"]["values"]["y"] = [2.0]
        mesh_steerable["coordsets"]["coords"]["values"]["z"] = [3.0]
    
        mesh_steerable["topologies"]["mesh"]["type"] = "unstructured"
        mesh_steerable["topologies"]["mesh"]["coordset"] = "coords"
        mesh_steerable["topologies"]["mesh"]["elements"]["shape"] = "point"
        mesh_steerable["topologies"]["mesh"]["elements"]["connectivity"] = [0]

        mesh_steerable["fields"]["learning_rate"]["association"] = "vertex"
        mesh_steerable["fields"]["learning_rate"]["topology"] = "mesh"
        mesh_steerable["fields"]["learning_rate"]["volume_dependent"] = "false"
        mesh_steerable["fields"]["learning_rate"]["values"] = [learning_rate]

        mesh_steerable["fields"]["parameter_x"]["association"] = "vertex"
        mesh_steerable["fields"]["parameter_x"]["topology"] = "mesh"
        mesh_steerable["fields"]["parameter_x"]["volume_dependent"] = "false"
        mesh_steerable["fields"]["parameter_x"]["values"] = [parameter_x]

        mesh_steerable["fields"]["parameter_y"]["association"] = "vertex"
        mesh_steerable["fields"]["parameter_y"]["topology"] = "mesh"
        mesh_steerable["fields"]["parameter_y"]["volume_dependent"] = "false"
        mesh_steerable["fields"]["parameter_y"]["values"] = [parameter_y]

# ========================================================================

        exec_params = node()
        exec_params["catalyst"]["state"]["time"] = epoch
        exec_params["catalyst"]["state"]["timestep"] = epoch
    
        # Name first channel "pred"
        exec_params["catalyst"]["channels"]["prediction"]["type"] = "mesh"
        exec_params["catalyst"]["channels"]["prediction"]["data"] = mesh_prediction
    
        # Name second channel "obj"
        exec_params["catalyst"]["channels"]["objective"]["type"] = "mesh"
        exec_params["catalyst"]["channels"]["objective"]["data"] = mesh_objective

        # Name third channel "loss"
        exec_params["catalyst"]["channels"]["loss"]["type"] = "mesh"
        exec_params["catalyst"]["channels"]["loss"]["data"] = mesh_loss
    
        # Name fourth channel "steerable"
        exec_params["catalyst"]["channels"]["steerable"]["type"] = "mesh"
        exec_params["catalyst"]["channels"]["steerable"]["data"] = mesh_steerable
    

# ========================================================================
        return catalyst.execute(exec_params.asDict())
